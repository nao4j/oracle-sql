CREATE TABLE clients (
  client_id  INTEGER PRIMARY KEY,
  first_name VARCHAR2(60) NOT NULL,
  last_name  VARCHAR(60)  NOT NULL,
  gendor     VARCHAR(10)  NOT NULL,
  birthday   DATE         NOT NULL,

  CONSTRAINT u_client_birthday UNIQUE (first_name, last_name, birthday)
);

CREATE SEQUENCE clients_seq;

CREATE OR REPLACE TRIGGER tik_clients
BEFORE INSERT ON clients
FOR EACH ROW
  BEGIN
    SELECT clients_seq.nextval
    INTO :new.client_id
    FROM dual;
  END;;

CREATE INDEX clients_ix ON clients (client_id);