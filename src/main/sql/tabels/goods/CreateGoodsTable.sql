CREATE TABLE goods (
  goods_id INTEGER PRIMARY KEY,
  name     VARCHAR(120) NOT NULL,

  CONSTRAINT u_goods_name UNIQUE (name)
);

CREATE SEQUENCE goods_seq;

CREATE OR REPLACE TRIGGER tik_goods
BEFORE INSERT ON goods
FOR EACH ROW
  BEGIN
    SELECT goods_seq.nextval
    INTO :new.goods_id
    FROM dual;
  END;;