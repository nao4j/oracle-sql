CREATE TABLE prices (
  price_id        INTEGER PRIMARY KEY,
  goods_id        INTEGER      NOT NULL,
  price           NUMBER(*, 2) NOT NULL,
  currency        VARCHAR2(3)  NOT NULL,
  effective_date  DATE         NOT NULL,
  expiration_date DATE         NOT NULL,

  CONSTRAINT fk_prices_goods FOREIGN KEY (goods_id) REFERENCES goods (goods_id),
  CONSTRAINT u_prices_eff_date UNIQUE (goods_id, currency, effective_date, expiration_date)
);

CREATE SEQUENCE prices_seq;

CREATE OR REPLACE TRIGGER tik_prices
BEFORE INSERT ON prices
FOR EACH ROW
  BEGIN
    SELECT prices_seq.nextval
    INTO :new.price_id
    FROM dual;
  END;;