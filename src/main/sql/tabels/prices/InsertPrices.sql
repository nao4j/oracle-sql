INSERT INTO prices (goods_id, price, currency, effective_date, expiration_date)
VALUES (
  (
    SELECT goods_id
    FROM goods
    WHERE name = 'Миксер'),
  5999.99,
  'RUR',
  TO_DATE('2012-09-15', 'yyyy-mm-dd'),
  TO_DATE('9999-01-01', 'yyyy-mm-dd')
);

INSERT INTO prices (goods_id, price, currency, effective_date, expiration_date)
VALUES (
  (
    SELECT goods_id
    FROM goods
    WHERE name = 'Топор'),
  1112.0,
  'RUR',
  TO_DATE('2015-01-19', 'yyyy-mm-dd'),
  TO_DATE('9999-01-01', 'yyyy-mm-dd')
);

INSERT INTO prices (goods_id, price, currency, effective_date, expiration_date)
VALUES (
  (
    SELECT goods_id
    FROM goods
    WHERE name = 'Карта Бобруйска'),
  59.99,
  'RUR',
  TO_DATE('2014-11-10', 'yyyy-mm-dd'),
  TO_DATE('9999-01-01', 'yyyy-mm-dd')
);


UPDATE prices
SET expiration_date = (TO_DATE('2016-03-20', 'yyyy-mm-dd'))
WHERE goods_id = (
  SELECT goods_id
  FROM goods
  WHERE name = 'Карта Бобруйска'
) AND expiration_date > TO_DATE('2016-03-20', 'yyyy-mm-dd');

INSERT INTO prices (goods_id, price, currency, effective_date, expiration_date)
VALUES (
  (
    SELECT goods_id
    FROM goods
    WHERE name = 'Карта Бобруйска'),
  39.99,
  'RUR',
  TO_DATE('2016-03-20', 'yyyy-mm-dd'),
  TO_DATE('9999-01-01', 'yyyy-mm-dd')
);