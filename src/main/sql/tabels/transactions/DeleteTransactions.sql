DELETE FROM transactions
WHERE client_id = (
  SELECT client_id
  FROM clients
  WHERE first_name = 'Артур' AND last_name = 'Пирожков' AND birthday = TO_DATE('1997-05-03', 'yyyy-mm-dd')
);

DELETE FROM transactions
WHERE client_id = (
  SELECT client_id
  FROM clients
  WHERE first_name = 'Валентина' AND last_name = 'Кривошеева' AND birthday = TO_DATE('1987-01-07', 'yyyy-mm-dd')
);