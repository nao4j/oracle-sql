CREATE TABLE transactions (
  transaction_id INTEGER PRIMARY KEY,
  client_id      INTEGER NOT NULL,
  goods_id       INTEGER NOT NULL,
  date_time      DATE    NOT NULL,

  CONSTRAINT fk_transactions_clients FOREIGN KEY (client_id) REFERENCES clients (client_id),
  CONSTRAINT fk_transactions_goods FOREIGN KEY (goods_id) REFERENCES goods (goods_id)
);

CREATE SEQUENCE transactions_seq;

CREATE OR REPLACE TRIGGER tik_transactions
BEFORE INSERT ON transactions
FOR EACH ROW
  BEGIN
    SELECT transactions_seq.nextval
    INTO :new.transaction_id
    FROM dual;
  END;;