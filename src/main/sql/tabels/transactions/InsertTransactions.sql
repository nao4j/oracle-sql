INSERT INTO transactions (client_id, goods_id, date_time)
VALUES (
  (
    SELECT client_id
    FROM clients
    WHERE first_name = 'Артур' AND last_name = 'Пирожков' AND birthday = TO_DATE('1997-05-03', 'yyyy-mm-dd')),
  (
    SELECT goods_id
    FROM goods
    WHERE name = 'Топор'),
  TO_DATE('2016-01-02 12:10', 'yyyy-mm-dd HH24:MI')
);

INSERT INTO transactions (client_id, goods_id, date_time)
VALUES (
  (
    SELECT client_id
    FROM clients
    WHERE first_name = 'Валентина' AND last_name = 'Кривошеева' AND birthday = TO_DATE('1987-01-07', 'yyyy-mm-dd')),
  (
    SELECT goods_id
    FROM goods
    WHERE name = 'Топор'),
  TO_DATE('2015-05-12 10:15', 'yyyy-mm-dd HH24:MI')
);

INSERT INTO transactions (client_id, goods_id, date_time)
VALUES (
  (
    SELECT client_id
    FROM clients
    WHERE first_name = 'Валентина' AND last_name = 'Кривошеева' AND birthday = TO_DATE('1987-01-07', 'yyyy-mm-dd')),
  (
    SELECT goods_id
    FROM goods
    WHERE name = 'Карта Бобруйска'),
  TO_DATE('2015-05-12 10:16', 'yyyy-mm-dd HH24:MI')
);

INSERT INTO transactions (client_id, goods_id, date_time)
VALUES (
  (
    SELECT client_id
    FROM clients
    WHERE first_name = 'Валентина' AND last_name = 'Кривошеева' AND birthday = TO_DATE('1987-01-07', 'yyyy-mm-dd')),
  (
    SELECT goods_id
    FROM goods
    WHERE name = 'Карта Бобруйска'),
  TO_DATE('2016-04-22 16:25', 'yyyy-mm-dd HH24:MI')
);