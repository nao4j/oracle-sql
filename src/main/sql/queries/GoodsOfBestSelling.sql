-- Задание: Вывести самый продаваемый товар
-- Если найдутся товары одинаковой продаваемости, выбрать самый дорогой

-- FIXME Не учтен выбор самого дорогого товара

SELECT
  temp2.goods_id,
  temp2.name,
  temp2.bay_cnt,
  p.price || ' ' || p.currency AS price
FROM (
       SELECT
         g.goods_id,
         g.name,
         temp.bay_cnt
       FROM goods g
         JOIN (
                SELECT
                  t.goods_id,
                  count(t.transaction_id) AS bay_cnt
                FROM transactions t
                GROUP BY t.goods_id
                ORDER BY bay_cnt
              ) temp ON g.goods_id = temp.goods_id) temp2
  JOIN prices p ON p.goods_id = temp2.goods_id
WHERE p.expiration_date >= SYSDATE
ORDER BY p.price DESC;
