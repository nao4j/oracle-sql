-- Задание: Вывести суммарные расходы каждого из клиентов с разбиением по годам и отсортировать по уменьшению суммы
-- Не включать результаты с суммой меньше 1к

SELECT
  temp.client_id,
  temp.year,
  SUM(temp.price) AS total_expenses
FROM (
       SELECT
         t.client_id,
         TO_CHAR(t.date_time, 'yyyy') AS year,
         p.price
       FROM transactions t
         JOIN prices p ON t.goods_id = p.goods_id
       WHERE t.date_time BETWEEN p.effective_date AND p.expiration_date) temp
GROUP BY temp.client_id, temp.year
HAVING SUM(temp.price) >= 1000
ORDER BY total_expenses DESC;