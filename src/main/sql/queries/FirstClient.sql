-- Задание: Вывести данные самого первого клиента, отформатировать дату его рождения по шаблону "dd.mm.yyyy"
-- и рассчитать текущий возраст

-- Решение через подзапросы
SELECT
  first_name,
  last_name,
  gendor,
  TO_CHAR(birthday, 'dd.mm.yyyy'),
  TO_NUMBER(TO_CHAR(SYSDATE, 'yyyy')) - TO_NUMBER(TO_CHAR(birthday, 'yyyy'))
FROM clients
WHERE client_id IN (
  SELECT client_id
  FROM transactions
  WHERE date_time = (
    SELECT min(date_time)
    FROM transactions
  )
);

-- Решение через джойны
SELECT
  c.first_name,
  c.last_name,
  c.gendor,
  TO_CHAR(c.birthday, 'dd.mm.yyyy'),
  TO_NUMBER(TO_CHAR(SYSDATE, 'yyyy')) - TO_NUMBER(TO_CHAR(c.birthday, 'yyyy'))
FROM clients c
  JOIN transactions t1
    ON c.client_id = t1.client_id
WHERE t1.date_time = (
  SELECT MIN(t2.date_time)
  FROM transactions t2
);