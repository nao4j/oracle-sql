-- Задание: Вывести id, имена и возраст клиентов и товаров в одной таблице

SELECT
  c.client_id                                                                  AS id,
  c.first_name || ' ' || c.last_name                                           AS name,
  TO_NUMBER(TO_CHAR(SYSDATE, 'yyyy')) - TO_NUMBER(TO_CHAR(c.birthday, 'yyyy')) AS age
FROM clients c
UNION
SELECT
  g.goods_id,
  g.name,
  TO_NUMBER(TO_CHAR(SYSDATE, 'yyyy')) - TO_NUMBER(TO_CHAR(p.effective_date, 'yyyy'))
FROM goods g
  JOIN prices p ON g.goods_id = p.goods_id
WHERE p.expiration_date >= SYSDATE;