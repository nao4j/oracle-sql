-- Задание: Вывести самый дорогой на данный момент товар продаваемый женщинам за 2015 год и актуальную цену

-- FIXME Не учтен выбор самого дорогого товара

SELECT
  goods_id,
  price
FROM prices
WHERE expiration_date = TO_DATE('9999.01.01', 'yyyy.mm.dd')
      AND goods_id IN (
  SELECT DISTINCT goods_id
  FROM transactions
  WHERE date_time BETWEEN TO_DATE('2015.01.01', 'yyyy.mm.dd') AND TO_DATE('2015.12.31', 'yyyy.mm.dd')
        AND client_id IN (
    SELECT client_id
    FROM clients
    WHERE gendor = 'female'
  )
);