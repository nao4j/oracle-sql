-- Задание: Вывести товар, который ещё ни кто не купил
-- Если есть несколько некупленных товаров, то выбрать самый "залежавшийся"

-- FIXME Не учтен выбор самого "залежавшийся" товара

SELECT
  trash_goods.goods_id,
  trash_goods.name,
  TO_NUMBER(TO_CHAR(SYSDATE, 'yyyy')) - TO_NUMBER(TO_CHAR(p.effective_date, 'yyyy')) AS age
FROM
  (
    SELECT
      g.goods_id,
      g.name
    FROM goods g
    WHERE g.goods_id NOT IN (
      SELECT DISTINCT t.goods_id
      FROM transactions t
    )) trash_goods
  JOIN prices p ON trash_goods.goods_id = p.goods_id
WHERE p.expiration_date >= SYSDATE
ORDER BY age DESC;